import { z } from 'zod'

// We're keeping a simple non-relational schema here.
// IRL, you will have a schema for your data models.
export const orderSchema = z.object({
  id: z.number(),
  doc_no: z.string(),
  issue_date: z.string(),
  due_date: z.string(),
  status: z.string(),
  payment_term: z.string(),
  order_amount: z.number(),
  warehouse: z.string(),
  down_payment: z.number(),
  input_by: z.string(),
  vendor_name: z.string(),

})

export type Order = z.infer<typeof orderSchema>
