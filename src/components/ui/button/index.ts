import { cva } from 'class-variance-authority'

export { default as Button } from './Button.vue'

export interface ButtonProps {
  variant?: NonNullable<Parameters<typeof buttonVariants>[0]>['variant']
  size?: NonNullable<Parameters<typeof buttonVariants>[0]>['size']
  as?: string
}

const buttonVar = {
  variants: {
    variant: {
      default: 'bg-primary text-primary-foreground hover:bg-primary/90',
      destructive: 'bg-destructive text-destructive-foreground hover:bg-destructive/90',
      outline: 'border border-input bg-background hover:bg-accent hover:text-accent-foreground',
      secondary: 'bg-secondary text-secondary-foreground hover:bg-secondary/80',
      ghost: 'hover:bg-accent hover:text-accent-foreground',
      export: 'bg-green-700 text-primary-foreground hover:bg-green-700/80',
      link: 'text-primary underline-offset-4 hover:underline',
    },
    size: {
      default: 'h-8 px-2 py-1',
      xs: 'h-5 px-1.5 py-0.5',
      sm: 'h-7 rounded-md px-2',
      lg: 'h-9 rounded-md px-6',
      icon: 'h-7 w-7',
    },
  },
}

export const buttonVariants = cva(
  'inline-flex items-center justify-center rounded-md text-sm font-medium ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50',
  {
    ...buttonVar,
    defaultVariants: {
      variant: 'default',
      size: 'default',
    },
  },
)
