export const statuses = [
  {
    value: 'pending',
    label: 'Pending',
  },
  {
    value: 'done',
    label: 'Done',
  },
  {
    value: 'voided',
    label: 'Voided',
  },
]

export const payment_terms = [
  {
    value: '1',
    label: 'Payment Later',
  },
  {
    value: '2',
    label: 'Payment in Advance',
  },
]
