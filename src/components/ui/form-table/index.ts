export { default as FormTable } from './FormTable.vue'
export { default as FormTableRow } from './FormTableRow.vue'
export { default as FormTableCell } from './FormTableCell.vue'
export type { FormTableCellProps } from './FormTableCell.vue'
export { default as FormTableInput } from './FormTableInput.vue'

export type FormTableCellCol = '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | '11' | '12' | string
export interface FormTableHeaderItem {
  label: string
  cols?: FormTableCellCol
  align?: 'left' | 'center' | 'right'
}
