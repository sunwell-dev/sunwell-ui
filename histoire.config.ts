import { defaultColors, defineConfig } from 'histoire'
import { HstVue } from '@histoire/plugin-vue'
import resolveConfig from 'tailwindcss/resolveConfig'
import tailwindConfig from './tailwind.config.js'

export default defineConfig({
  setupFile: './histoire.setup.ts',
  plugins: [HstVue()],
  theme: {
    title: 'Nexus UI',
    defaultColorScheme: 'light',
    logo: {
      square: './src/assets/logo.svg',
      dark: './src/assets/logo-text.svg',
      light: './src/assets/logo-text.svg',
    },
    colors: {
      gray: defaultColors.zinc,
      primary: defaultColors.blue,
    },
  },
  tree: {
    groups: [
      {
        id: 'top',
        title: '',
      },
      {
        id: 'components',
        title: 'Components',
        include: file => true,
      },
      {
        title: 'Example Pages',
        include: file => !file.title.includes('Pages Example'),
      },
    ],
  },
  outDir: 'public'
})
