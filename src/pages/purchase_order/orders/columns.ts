import { h } from 'vue'
import type { ColumnDef } from '@tanstack/vue-table'
import type { Order } from './schema'
import { statuses } from './data'
import { DataTableColumnHeader } from '@/components/ui/data-table'
import { Badge } from '@/components/ui/badge'

export const columns: ColumnDef<Order>[] = [
  {
    accessorKey: 'id',
    header: ({ column }) => h(DataTableColumnHeader, { column, title: 'No' }),
    cell: ({ row }) => h('div', { class: 'w-[10px]' }, row.getValue('id')),
    enableSorting: false,
    enableHiding: false,
  },
  {
    accessorKey: 'doc_no',
    header: ({ column }) => h(DataTableColumnHeader, { column, title: 'Purchase Order No'}),
    cell: ({ row }) => h('div', { class: 'w-[100px] text-blue-500 cursor-pointer font-semibold' }, row.getValue('doc_no')),
  },
  {
    accessorKey: 'issue_date',
    header: ({ column }) => h(DataTableColumnHeader, { column, title: 'Issue Date' }),
    cell: ({ row }) => {
      return h('div', { class: 'w-[80px]' }, row.getValue('issue_date'))
    },
  },
  {
    accessorKey: 'vendor_name',
    header: ({ column }) => h(DataTableColumnHeader, { column, title: 'Vendor Name' }),
    cell: ({ row }) => {
      return h('span', { class: 'w-[60px] truncate overflow-hidden font-medium', title: row.getValue('vendor_name') }, row.getValue('vendor_name'))
    },
  },
  {
    accessorKey: 'payment_term',
    header: ({ column }) => h(DataTableColumnHeader, { column, title: 'Payment Term' }),
    cell: ({ row }) => {
      const payment_term = row.getValue('payment_term') === 1 ? 'Payment Later' : 'Payment in Advance'
      return h('div', { class: 'truncate', title: payment_term }, payment_term)
    },
    filterFn: (row, id, value) => {
      const payment_term = row.getValue(id) === 1 ? '1' : '2'
      return value.includes(payment_term)
    },
  },
  {
    accessorKey: 'order_amount',
    header: ({ column }) => h(DataTableColumnHeader, { column, title: 'Order Amount' }),
    cell: ({ row }) => {
      return h('div', { class: 'text-right' }, row.getValue('order_amount'))
    },
  },
  {
    accessorKey: 'warehouse',
    header: ({ column }) => h(DataTableColumnHeader, { column, title: 'Warehouse' }),
    cell: ({ row }) => h('div', row.getValue('warehouse')),
  },
  {
    accessorKey: 'down_payment',
    header: ({ column }) => h(DataTableColumnHeader, { column, title: 'Down Payment' , class: 'justify-end'}),
    cell: ({ row }) => h('div', { class: 'text-right' }, row.getValue('down_payment')),
  },
  {
    accessorKey: 'due_date',
    header: ({ column }) => h(DataTableColumnHeader, { column, title: 'Expired Date' }),
    cell: ({ row }) => h('div', row.getValue('due_date')),
  },
  {
    accessorKey: 'input_by',
    header: ({ column }) => h(DataTableColumnHeader, { column, title: 'Input By' }),
    cell: ({ row }) => h('span', { class: 'max-w-[200px] truncate' }, row.getValue('input_by')),
  },
  {
    accessorKey: 'status',
    header: ({ column }) => h(DataTableColumnHeader, { column, title: 'Status' }),
    cell: ({ row }) => {
      const status = statuses.find(
        status => status.value === row.getValue('status'),
      )

      if (!status)
        return null

      return h(Badge, { class: 'flex w-[100px] items-center' }, () => status.label)
    },
    filterFn: (row, id, value) => {
      return value.includes(row.getValue(id))
    },
    enableSorting: false,
    enableHiding: false,
  },
]
