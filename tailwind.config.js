import tailwindcssAnimate from 'tailwindcss-animate'

/** @type {import('tailwindcss').Config} */
export default {
  darkMode: 'class',
  content: [
    './index.html',
    './src/**/*.{vue,js,ts,jsx,tsx,md}',
    './.vitepress/**/*.{vue,js,ts,jsx,tsx}',
  ],
  theme: {
    container: {
      center: true,
      padding: '2rem',
      screens: {
        '2xl': '1400px',
      },
    },
    fontSize: {
      'xs': '0.625rem',
      'sm': '0.8rem',
      'base': '1rem',
      'xl': '1.25rem',
      '2xl': '1.563rem',
      '3xl': '1.953rem',
      '4xl': '2.441rem',
      '5xl': '3.052rem',
    },
    extend: {
      fontFamily: {
        sans: ['Inter', 'ui-sans-serif', 'system-ui'],
      },
      colors: {
        border: 'hsl(var(--border))',
        input: 'hsl(var(--input))',
        ring: 'hsl(var(--ring))',
        background: 'hsl(var(--background))',
        foreground: 'hsl(var(--foreground))',
        primary: {
          DEFAULT: 'hsl(var(--primary))',
          foreground: 'hsl(var(--primary-foreground))',
        },
        secondary: {
          DEFAULT: 'hsl(var(--secondary))',
          foreground: 'hsl(var(--secondary-foreground))',
        },
        destructive: {
          DEFAULT: 'hsl(var(--destructive))',
          foreground: 'hsl(var(--destructive-foreground))',
        },
        muted: {
          DEFAULT: 'hsl(var(--muted))',
          foreground: 'hsl(var(--muted-foreground))',
        },
        accent: {
          DEFAULT: 'hsl(var(--accent))',
          foreground: 'hsl(var(--accent-foreground))',
        },
        popover: {
          DEFAULT: 'hsl(var(--popover))',
          foreground: 'hsl(var(--popover-foreground))',
        },
        card: {
          DEFAULT: 'hsl(var(--card))',
          foreground: 'hsl(var(--card-foreground))',
        },
      },
      borderRadius: {
        lg: 'var(--radius)',
        md: 'calc(var(--radius) - 2px)',
        sm: 'calc(var(--radius) - 4px)',
      },
      boxShadow: {
        switch: 'rgba(0, 0, 0, 0.3) 0px 0px 1px, rgba(0, 0, 0, 0.2) 0px 1px 2px',
      },
      keyframes: {
        'accordion-down': {
          from: { height: 0 },
          to: { height: 'var(--radix-accordion-content-height)' },
        },
        'accordion-up': {
          from: { height: 'var(--radix-accordion-content-height)' },
          to: { height: 0 },
        },
        'collapsible-down': {
          from: { height: 0 },
          to: { height: 'var(--radix-collapsible-content-height)' },
        },
        'collapsible-up': {
          from: { height: 'var(--radix-collapsible-content-height)' },
          to: { height: 0 },
        },
        'slide-down': {
          from: { height: 0 },
          to: { height: '12px' },
        },
        'slide-up': {
          from: { height: '12px' },
          to: { height: 0 },
        },

      },
      animation: {
        'accordion-down': 'accordion-down 0.2s ease-in-out',
        'accordion-up': 'accordion-up 0.2s ease-in-out',
        'collapsible-down': 'collapsible-down 0.2s ease-in-out',
        'collapsible-up': 'collapsible-up 0.2s ease-in-out',
        'slide-down': 'slide-down 0.2s ease-in-out',
        'slide-up': 'slide-up 0.3s ease-in-out',
      },
      flex: {
        'col-1': '0 0 8.3333333333%',
        'col-2': '0 0 16.6666666667%',
        'col-3': '0 0 25%',
        'col-4': '0 0 33.3333333333%',
        'col-5': '0 0 41.6666666667%',
        'col-6': '0 0 50%',
        'col-7': '0 0 58.3333333333%',
        'col-8': '0 0 66.6666666667%',
        'col-9': '0 0 75%',
        'col-10': '0 0 83.3333333333%',
        'col-11': '0 0 91.6666666667%',
        'col-12': '0 0 100%',
      },
      // maxWidth: {
      //   'col-1': '8.3333333333%',
      //   'col-2': '16.6666666667%',
      //   'col-3': '25%',
      //   'col-4': '33.3333333333%',
      //   'col-5': '41.6666666667%',
      //   'col-6': '50%',
      //   'col-7': '58.3333333333%',
      //   'col-8': '66.6666666667%',
      //   'col-9': '75%',
      //   'col-10': '83.3333333333%',
      //   'col-11': '91.6666666667%',
      //   'col-12': '100%',
      // },
    },
  },
  plugins: [tailwindcssAnimate],
}
