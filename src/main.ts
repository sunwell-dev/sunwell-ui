import { createApp } from 'vue'
import './style.css'
import { createRouter, createWebHashHistory } from 'vue-router'
import App from './App.vue'
import Purchase from './pages/Purchase.vue'
import PurchaseOrderlist from './pages/purchase_order/PurchaseOrderList.vue'
import AddPurchaseOrderVue from './pages/purchase_order/AddPurchaseOrder.vue'

const routes = [
  {
    path: '/',
    component: Purchase,
  },
  {
    path: '/purchase_order',
    component: PurchaseOrderlist,
  },
  {
    path: '/add/purchase_order',
    component: AddPurchaseOrderVue,
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

const app = createApp(App)
app.use(router)
app.mount('#app')
